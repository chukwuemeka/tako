from __future__ import absolute_import

from django.core.cache import caches
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        caches['site'].clear()
