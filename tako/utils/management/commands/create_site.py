from __future__ import absolute_import

import logging

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.management.base import BaseCommand

SITE_DOMAIN = getattr(settings, 'SITE_DOMAIN')
SITE_NAME = getattr(settings, 'SITE_NAME')
SITE_ID = getattr(settings, 'SITE_ID')
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        site, is_created = Site.objects.update_or_create(id=SITE_ID, defaults={
            'domain': SITE_DOMAIN,
            'name': SITE_NAME
        })
        logger.info('Updated site entry %s, %s', site.name, site.domain)
