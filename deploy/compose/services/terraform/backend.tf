terraform {
  backend "s3" {
    bucket = "tako-production"
    key    = "terraform/terraform.tfstate"
    region = "us-east-1"
  }
}