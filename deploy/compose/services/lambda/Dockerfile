FROM lambci/lambda:python3.6

MAINTAINER Chukwuemeka Ezekwe <emeka@ez3softworks.com>

USER root

RUN yum makecache fast
RUN yum update
RUN yum install -y zip
RUN yum install -y unzip
RUN yum install -y git
RUN yum install -y gcc gcc-c++

# Install GEOS
WORKDIR /tmp
RUN curl -O http://download.osgeo.org/geos/geos-3.4.2.tar.bz2
RUN tar xjf geos-3.4.2.tar.bz2
WORKDIR geos-3.4.2
RUN ./configure
RUN make
RUN make install

# Install PROJ
WORKDIR /tmp
RUN curl -O http://download.osgeo.org/proj/proj-4.9.1.tar.gz
RUN curl -O http://download.osgeo.org/proj/proj-datumgrid-1.5.tar.gz
RUN tar xzf proj-4.9.1.tar.gz
WORKDIR /tmp/proj-4.9.1/nad
RUN tar xzf ../../proj-datumgrid-1.5.tar.gz
WORKDIR /tmp/proj-4.9.1
RUN ./configure
RUN make
RUN make install

# Install GDAL
WORKDIR /tmp
RUN curl -O http://download.osgeo.org/gdal/1.11.2/gdal-1.11.2.tar.gz
RUN tar xzf gdal-1.11.2.tar.gz
WORKDIR /tmp/gdal-1.11.2
RUN ./configure
RUN make
RUN make install

# install our code
ADD requirements.txt /var/task/
RUN pip3 install -r /var/task/requirements.txt
ADD . /var/task/
ADD deploy/compose/services/lambda/lib/* /var/task/lib/

# install virtualenv
RUN pip3 install virtualenv

ENTRYPOINT []
CMD ["bash"]