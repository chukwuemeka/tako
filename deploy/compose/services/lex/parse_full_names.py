from zipfile import ZipFile
import re
import random
import csv
import yaml


last_names = []
with open('./data/last_names.csv') as csv_file:
    for i, row in enumerate(csv.reader(csv_file)):
        if i == 0:
            continue
        last_names.append(row[0])


male_first_names = {}
female_first_names = {}
zip_file = ZipFile('./data/names.zip')
for name in zip_file.namelist():
    if not re.match('^yob19[9876]\d', name):
        continue
    with zip_file.open(name) as text_file:
        for line in text_file.readlines():
            text = line.decode('utf-8').strip()
            tokens = text.split(',')

            name = tokens[0]
            gender = tokens[1]
            total = int(tokens[2])
            if gender == 'M':
                if name not in male_first_names:
                    male_first_names[name] = 0
                male_first_names[name] += total
            elif gender == 'F':
                if name not in female_first_names:
                    female_first_names[name] = 0
                female_first_names[name] += total

male_first_names = sorted(male_first_names, key=male_first_names.get, reverse=True)[:int(len(male_first_names) * .25)]
female_first_names = sorted(female_first_names, key=female_first_names.get, reverse=True)[:int(len(male_first_names) * .25)]

full_names = [
    'Chukwuemeka Ezekwe'
]
for i in range(9999):
    first_name = random.choice(random.choice([male_first_names, female_first_names])).capitalize()
    last_name = random.choice(last_names).capitalize()
    full_names.append('%s %s' % (first_name, last_name))

with open('full_names.yaml', 'w') as yaml_file:
    data = {
        'slot_types': {
            'full_name': {
                'name': 'FullName',
                'enumeration_values': full_names
            }
        }
    }
    yaml_file.write(yaml.dump(data, default_flow_style=False))
